# Checkout Branch #

A quick script for checking out a new branch based off user, issue prefix and issue number

### Install ###

* Copy ```b``` to /usr/local/bin, or rename if wanted
* ```chmod +x /usr/local/bin/b```
* Add ```.fpc``` to your global gitignore
  * If there is no global gitignore, one can be setup with the following:
    * ```git config --global core.excludesFile '~/.gitignore_global'```
	* ```echo .fpc >> ~/.gitignore_global```

### Create .fpc file in root of repository ###

*.fpc*


```shell
FPC_USER=jer
FPC_PREFIX=e2l
```

### Usage ###

#### To checkout a new branch for an issue number:


```shell
b 100
```

#### This will checkout a branch to the configured user, prefix and issue number:


```shell
Checking out branch: jer/e2l-1
Switched to a new branch 'jer/e2l-1'
```

- If the branch already exists, the script will just switch to the branch.
- If the current branch already is the specified branch, nothing will change.