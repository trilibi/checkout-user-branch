#!/usr/bin/env bash

if [ -f .fpc ]; then
    export $(cat .fpc | grep -v '#' | awk '/=/ {print $1}')
else
    echo "Missing .fpc file"
    echo "This command requires an .fpc file in the root of the repository with the following:"
    echo "----"
    echo "FPC_USER=name"
    echo "FPC_PREFIX=e2l"
    echo "----"
    echo "# FPC_USER:    Username used for creating branch"
    echo "# FPC_PREFIX:  branch prefix"
    exit 1
fi

if [  "$1" = "" ]; then
    echo "Please pass in an issue number"
    echo "example: b 352"
    exit 1
fi

BRANCH_NAME="$FPC_USER/$FPC_PREFIX-$1"
CURRENT_BRANCH="$(git branch --show-current)"

if [ "$CURRENT_BRANCH" = "$BRANCH_NAME" ]; then
    echo "Already on branch"
    exit 1;
fi

if [ `git branch --list $BRANCH_NAME` ]
then
   echo "$BRANCH_NAME already exists.  Switching to it now"
   git checkout $BRANCH_NAME
else
   echo Checking out branch: $BRANCH_NAME;
   git checkout -b $BRANCH_NAME
fi
